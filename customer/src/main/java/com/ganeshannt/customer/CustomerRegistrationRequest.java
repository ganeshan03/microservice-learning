package com.ganeshannt.customer;

public record CustomerRegistrationRequest(
        String firstName,
        String lastName,
        String email) {
}
