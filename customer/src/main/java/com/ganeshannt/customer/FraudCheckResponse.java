package com.ganeshannt.customer;

public record FraudCheckResponse(boolean isFraud) {
}
