package com.ganeshannt.fraud;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/fraud-check")
@Slf4j
public record FraudController(FraudCheckService fraudService) {

    @GetMapping("/{customerId}")
    public FraudCheckResponse fraudCheck(@PathVariable("customerId") Integer customerId) {
        log.info("fraud controller - Inside fraudCheck");
        boolean isFraudCustomer = fraudService.isFraudCustomer(customerId);
        return new FraudCheckResponse(isFraudCustomer);
    }
}
