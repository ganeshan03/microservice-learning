package com.ganeshannt.fraud;

public record FraudCheckResponse(boolean isFraud) {
}
