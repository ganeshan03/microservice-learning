package com.ganeshannt.fraud;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public record FraudCheckService(FraudCheckHistoryRepository fraudRepository) {

    public boolean isFraudCustomer(Integer customerId) {
        fraudRepository.save(FraudCheckHistory.builder()
                .customerId(customerId)
                .isFraud(false)
                .createdAt(LocalDateTime.now())
                .build()
        );
        return false;
    }
}
